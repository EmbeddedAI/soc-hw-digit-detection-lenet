#include <stdio.h>
#include <math.h>

float bitsToFloat(unsigned long numBin) {
    // 1 sign bit | 8 exponent bit | 23 fraction bits 
    float result=0,mantissa=1,numE=0;
    int i;
    for (i=0;i<=7;++i)
        numE += ((numBin & 1<<23+i)>>23+i)*pow(2,i);
    for (i=1;i<=23;++i)
        mantissa += ((numBin & 1<<23-i)>>23-i)*pow(2,-i);
    result=pow(2,numE-127)*mantissa;
    if(numBin & 1<<31)
        result *= -1.0;
    return result;
}

int main(){
	unsigned long numBin = 0b00111101000011001010101001000101;
    printf("%.19f\n",bitsToFloat(numBin));
}