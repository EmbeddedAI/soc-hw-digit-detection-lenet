#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>

#include "alt_types.h"

// === FPGA side ===
#define HW_REGS_BASE ( 0xFF200000 )
#define HW_REGS_SPAN ( 0x00200000 ) 
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )
#define DOT_PRODUCT_BASE 0x0
#define DIV32_BASE 0x80
#define HEX_5_BASE 0xa0
#define HEX_4_BASE 0xb0
#define HEX_3_BASE 0xc0
#define HEX_2_BASE 0xd0
#define HEX_1_BASE 0xe0
#define HEX_0_BASE 0xf0
#define BUTTONS_0_BASE 0x100
#define SWITCHES_0_BASE 0x110
#define LEDS_0_BASE 0x120
#define REG_AVALON_0_BASE 0x0

/*"register offset definitions */
#define DVND_REG_OFT 0 // dividend register address offset
#define DVSR_REG_OFT 1 // divisor register address offset
#define STRT_REG_OFT 2 // start register address offset
#define QUOT_REG_OFT 3 // quotient register address offset
#define REMN_REG_OFT 4 // remainder register address offset
#define REDY_REG_OFT 5 // ready signal register address offset
#define DONE_REG_OFT 6 // done_tick register address offset

/* main program */
int main(){
    // === FPGA Peripherals ===
    int fd; //Variable used to store /dev/mem pointer
    void *h2p_lw_virtual_base=NULL; //to store virtual base of HPS2FPGA lightweight bridge
    volatile unsigned int *h2p_lw_div_32_addr=NULL; //to Store div 32 dvnd reg address
    //=== get virtual addresses ===============
    // Open /dev/mem
	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf("ERROR: could not open \"/dev/mem\".\n");
        printf("    errno = %s\n", strerror(errno));
        exit(EXIT_FAILURE);
	}

	// get virtual addr that maps to physical address
	h2p_lw_virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );	
	if( h2p_lw_virtual_base == MAP_FAILED ) {
		printf("Error: h2p_lw_virtual_base mmap() failed.\n");
        printf("    errno = %s\n", strerror(errno));
        close(fd);
        exit(EXIT_FAILURE);
	} // end if
	
	// Get the address that maps to the FPGA peripherals
    h2p_lw_div_32_addr=(unsigned int *)(h2p_lw_virtual_base + DIV32_BASE);

    alt_u32 a, b, q, r, ready, done;
    printf("Division accelerator test: \n\n");
    while(1){
        printf("Perform division a / b = q remainder r\n");
        printf("Enter a: ");
        scanf("%lu", &a);
        printf("Enter b: ");
        scanf("%lu", &b);

        /* send data to division accelerator */
        *(h2p_lw_div_32_addr+DVND_REG_OFT)=a;  // IOWR(DIV32_BASE, DVND_REG_OFT, a);
        *(h2p_lw_div_32_addr+DVSR_REG_OFT)=b; // IOWR(DIV32_BASE, DVSR_REG_OFT, b);

        /* wait until the division accelerator is ready */
        while(1){
            ready=*(h2p_lw_div_32_addr+REDY_REG_OFT);  // ready = IORD(DIV32_BASE, REDY_REG_OFT) & 0x00000001;
            if(ready)
                break;
        }

        /* generate a 1-pulse */
        printf("Start... \n");
        *(h2p_lw_div_32_addr+STRT_REG_OFT)=1;  // IOWR(DIV32_BASE, STRT_REG_OFT, 1);

        /* wait for completion */
        while(1){
            done=*(h2p_lw_div_32_addr+DONE_REG_OFT);  // done = IORD(DIV32_BASE, DONE_REG_OFT) & 0x00000001;
            if(done)
                break;
        }

        /* clear done-tick register */
        *(h2p_lw_div_32_addr+DONE_REG_OFT)=1;  // IOWR(DIV32_BASE, DONE_REG_OFT, 1);

        /* retrieve results from division accelerator */
        q=*(h2p_lw_div_32_addr+QUOT_REG_OFT);  // q = IORD(DIV32_BASE, QUOT_REG_OFT);
        r=*(h2p_lw_div_32_addr+REMN_REG_OFT);  // r = IORD(DIV32_BASE, REMN_REG_OFT);
        printf("Hardware: %lu / %lu = %lu remainder %lu\n",a, b, q, r);

        /* compare results with built-in C operators */
        printf("Software: %lu / %lu = %lu remainder %lu\n\n",a, b, a / b, a % b);
    }// infinite loop
    return 0;
}// main