#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>


// === FPGA side ===
#define HW_REGS_BASE ( 0xFF200000 )
#define HW_REGS_SPAN ( 0x00200000 ) 
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )
#define HEX_5_BASE 0x0
#define HEX_4_BASE 0x10
#define HEX_3_BASE 0x20
#define HEX_2_BASE 0x30
#define HEX_1_BASE 0x40
#define HEX_0_BASE 0x50
#define BUTTONS_0_BASE 0x60
#define SWITCHES_0_BASE 0x70
#define LEDS_0_BASE 0x80
#define REG_AVALON_0_BASE 0x0

#define STRT_REG_OFT 1
#define REDY_REG_OFT 2
#define DONE_REG_OFT 3
#define CLR_REG_OFT  4


// === HPS side ===
#define GPIO1_BASE 0xFF709000
#define GPIO1_EXT_OFFSET 0x14
// the bit position of the switch
#define bit_24 0x01000000
#define bit_25 0x02000000
// ===============


// the position of the green led
#define bit_24 0x01000000
// the postion of the gpio1 reset bit
#define bit_26 0x04000000

int hex_lut[]={0x40, 0x79, 0x24, 0x30, 0x19, 0x12, 0x02, 0x78,
               0x00, 0x10, 0x08, 0x03, 0x46, 0x21, 0x06, 0x0e};

int main()  {
    // === FPGA Peripherals ===
    int fd; //Variable used to store /dev/mem pointer
    void *h2p_lw_virtual_base=NULL; //to store virtual base of HPS2FPGA lightweight bridge
	volatile unsigned int *h2p_lw_hex_0_addr=NULL; //to Store hex0 address
    volatile unsigned int *h2p_lw_hex_1_addr=NULL; //to Store hex1 address
    volatile unsigned int *h2p_lw_hex_2_addr=NULL; //to Store hex2 address
    volatile unsigned int *h2p_lw_hex_3_addr=NULL; //to Store hex3 address
    volatile unsigned int *h2p_lw_hex_4_addr=NULL; //to Store hex4 address
    volatile unsigned int *h2p_lw_hex_5_addr=NULL; //to Store hex5 address
    volatile unsigned int *h2p_lw_button_addr=NULL; //to Store buttons pio address
	volatile unsigned int *h2p_lw_leds_addr=NULL; //to Store led pio address
    volatile unsigned int *h2p_lw_switch_addr=NULL; //to Store switches pio address
	
	volatile int  i;
	
	volatile int quo100000, rem100000;
	volatile int quo10000, rem10000;
	volatile int quo1000, rem1000;
	volatile int quo100, rem100;
	volatile int quo10, rem10;
    
    volatile int sw_reading;
    
    printf( "Manage LEDs, Switches and 7-seg Displays in the FPGA\n");

    //=== get virtual addresses ===============
    // Open /dev/mem
	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if(fd==-1){
		printf("ERROR: could not open \"/dev/mem\".\n");
        printf("    errno = %s\n", strerror(errno));
        exit(EXIT_FAILURE);
	}

    
	// get virtual addr that maps to physical address
	h2p_lw_virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );	
	if( h2p_lw_virtual_base == MAP_FAILED ) {
		printf("Error: h2p_lw_virtual_base mmap() failed.\n");
        printf("    errno = %s\n", strerror(errno));
        close(fd);
        exit(EXIT_FAILURE);
	}
	
	// Get the address that maps to the FPGA peripherals
    h2p_lw_leds_addr=(unsigned int *)(h2p_lw_virtual_base + LEDS_0_BASE);
    h2p_lw_hex_0_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_0_BASE);
	h2p_lw_hex_1_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_1_BASE);
	h2p_lw_hex_2_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_2_BASE);
	h2p_lw_hex_3_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_3_BASE);
	h2p_lw_hex_4_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_4_BASE);
	h2p_lw_hex_5_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_5_BASE);
	h2p_lw_button_addr=(unsigned int *)(h2p_lw_virtual_base + BUTTONS_0_BASE);
	h2p_lw_switch_addr=(unsigned int *)(h2p_lw_virtual_base + SWITCHES_0_BASE);
	
	while(1){
		sw_reading =  *h2p_lw_switch_addr; //Read the word from switches
		*h2p_lw_leds_addr = sw_reading; //each led will be controlled by its correspondent switch
		
		i = sw_reading;
		quo100000 = i / 100000;
		rem100000 = i % 100000;
		quo10000 =  rem100000 / 10000;
		rem10000 =  rem100000 % 10000;
		quo1000 = rem10000 / 1000;
		rem1000 = rem10000 % 1000;
		quo100 = rem1000 / 100;
		rem100 = rem1000 % 100;
		quo10 = rem100 / 10;
		rem10 = rem100 % 10;
	
		*h2p_lw_hex_5_addr = hex_lut[quo100000]; // Turn off HEX
		*h2p_lw_hex_4_addr = hex_lut[quo10000]; // Turn off HEX
		*h2p_lw_hex_3_addr = hex_lut[quo1000]; // Turn off HEX
		*h2p_lw_hex_2_addr = hex_lut[quo100]; // Turn off HEX
		*h2p_lw_hex_1_addr = 0xFF; // Turn off HEX
		*h2p_lw_hex_1_addr = hex_lut[quo10]; // Turn off HEX
		*h2p_lw_hex_0_addr = hex_lut[rem10];
		
		//printf("Switches = %d; Result of BCD convertion: %d %d %d %d %d %d \n",sw_reading, quo100000, quo10000, quo1000, quo100, quo10, rem10);


	}

	close( fd );
	return 0;
}
