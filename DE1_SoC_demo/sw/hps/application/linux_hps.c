#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>


// === FPGA side ===
#define HW_REGS_BASE ( 0xFF200000 )
#define HW_REGS_SPAN ( 0x00200000 ) 
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )
#define HEX_5_BASE 0x0
#define HEX_4_BASE 0x10
#define HEX_3_BASE 0x20
#define HEX_2_BASE 0x30
#define HEX_1_BASE 0x40
#define HEX_0_BASE 0x50
#define BUTTONS_0_BASE 0x60
#define SWITCHES_0_BASE 0x70
#define LEDS_0_BASE 0x80
#define REG_AVALON_0_BASE 0x0

#define STRT_REG_OFT 1
#define REDY_REG_OFT 2
#define DONE_REG_OFT 3
#define CLR_REG_OFT  4


// === HPS side ===
#define GPIO1_BASE 0xFF709000
#define GPIO1_EXT_OFFSET 0x14
// the bit position of the switch
#define bit_24 0x01000000
#define bit_25 0x02000000
// ===============


// the position of the green led
#define bit_24 0x01000000
// the postion of the gpio1 reset bit
#define bit_26 0x04000000

int hex_lut[]={0x40, 0x79, 0x24, 0x30, 0x19, 0x12, 0x02, 0x78,
				0x00, 0x10, 0x08, 0x03, 0x46, 0x21, 0x06, 0x0e};

//prototype functions
int is_fpga_button_pressed(int, volatile unsigned int*);
void number_switches(int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *);
void led_flashing_workshop1(int*, volatile unsigned int *, volatile unsigned int *);
void reset_peripherals_out(volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *, volatile unsigned int *);
void sw_get_command(volatile unsigned int*, volatile unsigned int *, int*, char*, char*, char*);
void led_chasing(volatile unsigned int*, int, char, char*, char*);
void led_collision(volatile unsigned int*, int, char);
void workshop2(int*, volatile unsigned int*, volatile unsigned int*, volatile unsigned int *, volatile unsigned int*, volatile unsigned int*, volatile unsigned int *, volatile unsigned int*, volatile unsigned int*, volatile unsigned int*, char *, char *);


int main()  {
    // === FPGA Peripherals ===
    int fd; //Variable used to store /dev/mem pointer
    void *h2p_lw_virtual_base=NULL; //to store virtual base of HPS2FPGA lightweight bridge
	volatile unsigned int *h2p_lw_hex_0_addr=NULL; //to Store hex0 address
    volatile unsigned int *h2p_lw_hex_1_addr=NULL; //to Store hex1 address
    volatile unsigned int *h2p_lw_hex_2_addr=NULL; //to Store hex2 address
    volatile unsigned int *h2p_lw_hex_3_addr=NULL; //to Store hex3 address
    volatile unsigned int *h2p_lw_hex_4_addr=NULL; //to Store hex4 address
    volatile unsigned int *h2p_lw_hex_5_addr=NULL; //to Store hex5 address
    volatile unsigned int *h2p_lw_button_addr=NULL; //to Store buttons pio address
	volatile unsigned int *h2p_lw_leds_addr=NULL; //to Store led pio address
    volatile unsigned int *h2p_lw_switch_addr=NULL; //to Store switches pio address
    //=== get virtual addresses ===============
    // Open /dev/mem
	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf("ERROR: could not open \"/dev/mem\".\n");
        printf("    errno = %s\n", strerror(errno));
        exit(EXIT_FAILURE);
	}

	// get virtual addr that maps to physical address
	h2p_lw_virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );	
	if( h2p_lw_virtual_base == MAP_FAILED ) {
		printf("Error: h2p_lw_virtual_base mmap() failed.\n");
        printf("    errno = %s\n", strerror(errno));
        close(fd);
        exit(EXIT_FAILURE);
	} // end if
	
	// Get the address that maps to the FPGA peripherals
    h2p_lw_leds_addr=(unsigned int *)(h2p_lw_virtual_base + LEDS_0_BASE);
    h2p_lw_hex_0_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_0_BASE);
	h2p_lw_hex_1_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_1_BASE);
	h2p_lw_hex_2_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_2_BASE);
	h2p_lw_hex_3_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_3_BASE);
	h2p_lw_hex_4_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_4_BASE);
	h2p_lw_hex_5_addr=(unsigned int *)(h2p_lw_virtual_base + HEX_5_BASE);
	h2p_lw_button_addr=(unsigned int *)(h2p_lw_virtual_base + BUTTONS_0_BASE);
	h2p_lw_switch_addr=(unsigned int *)(h2p_lw_virtual_base + SWITCHES_0_BASE);
	int change_function, flag_function = 1, selector = 0, selector_in = 1;
	char shift = -1, flag_address = 1; // flag_addres: 1 - Left | 0 - Right
	printf("Bienvenido a SoC en Arm en Ubuntu 16.04\n");
	while(selector != -1){
		change_function = is_fpga_button_pressed(3,h2p_lw_button_addr);
		if(!change_function || flag_function){
			printf("Seleccione la función:\n");
			printf("Nota: Oprima -1 si desea Salir\n");
			printf("	1. Función test Soc en linux.\n");
			printf("	2. Función del taller 1.\n");
			printf("	3. Función del taller 2.\n");
			printf("Selección: ");
			scanf("%d", &selector);	
			selector_in = 1;
			flag_function = 0;
			reset_peripherals_out(h2p_lw_hex_5_addr, h2p_lw_hex_4_addr, h2p_lw_hex_3_addr, h2p_lw_hex_2_addr, h2p_lw_hex_1_addr, h2p_lw_hex_0_addr, h2p_lw_leds_addr);
		} // end if
		if(!flag_function){
			switch (selector){
				case 1:
					number_switches(&selector_in, h2p_lw_hex_5_addr, h2p_lw_hex_4_addr, h2p_lw_hex_3_addr, h2p_lw_hex_2_addr, h2p_lw_hex_1_addr, h2p_lw_hex_0_addr, h2p_lw_switch_addr, h2p_lw_leds_addr, h2p_lw_button_addr);
					break;
				case 2:
					led_flashing_workshop1(&selector_in, h2p_lw_switch_addr, h2p_lw_leds_addr);
					break;
				case 3:
					workshop2(&selector_in, h2p_lw_switch_addr, h2p_lw_button_addr, h2p_lw_hex_5_addr, h2p_lw_hex_4_addr, h2p_lw_hex_3_addr, h2p_lw_hex_2_addr, h2p_lw_hex_1_addr, h2p_lw_hex_0_addr, h2p_lw_leds_addr, &shift, &flag_address);
					break;
				case -1:
					printf("Bye...\n");
					break;
				default:
					printf("No selecciono una opción correcta.\n");
					flag_function = 1;
					break;
			} // end switch
		} // end if
	} // end while
} // end main

int is_fpga_button_pressed(int button_number, volatile unsigned int *base){
	//buttons are active low
	return (*base)&(1<<button_number);
} // end is_fpga_button_pressed

void number_switches(int *selector, volatile unsigned int *h2p_lw_hex_5_addr, volatile unsigned int *h2p_lw_hex_4_addr, volatile unsigned int *h2p_lw_hex_3_addr, volatile unsigned int *h2p_lw_hex_2_addr, volatile unsigned int *h2p_lw_hex_1_addr, volatile unsigned int *h2p_lw_hex_0_addr, volatile unsigned int *h2p_lw_switch_addr, volatile unsigned int *h2p_lw_leds_addr, volatile unsigned int *h2p_lw_button_addr){
	volatile int  i;
	static int pause = 0;
	static int pause_end = 1;
	int value_read;
	volatile int quo100000, rem100000;
	volatile int quo10000, rem10000;
	volatile int quo1000, rem1000;
	volatile int quo100, rem100;
	volatile int quo10, rem10;
    volatile int sw_reading;
	if(*selector){
		printf( "Manage LEDs, Switches and 7-seg Displays in the FPGA\n");
		*selector = 0;
	} // end if
	if(!is_fpga_button_pressed(0,h2p_lw_button_addr)){
		pause = 0;
		pause_end = 1;
	} // end if
	if(is_fpga_button_pressed(2,h2p_lw_button_addr)){
		if(is_fpga_button_pressed(1,h2p_lw_button_addr)){
			sw_reading =  *h2p_lw_switch_addr; //Read the word from switches
		}else{
			printf("Ingrese el numero a encender: ");
			scanf("%d", &sw_reading);
			pause = 1;
		} // end if
		if(pause_end){
			if(pause)
				pause_end = 0;
			*h2p_lw_leds_addr = sw_reading; //each led will be controlled by its correspondent switch
			i = sw_reading;
			quo100000 = i / 100000;
			rem100000 = i % 100000;
			quo10000 =  rem100000 / 10000;
			rem10000 =  rem100000 % 10000;
			quo1000 = rem10000 / 1000;
			rem1000 = rem10000 % 1000;
			quo100 = rem1000 / 100;
			rem100 = rem1000 % 100;
			quo10 = rem100 / 10;
			rem10 = rem100 % 10;
			*h2p_lw_hex_5_addr = hex_lut[quo100000]; // Turn off HEX
			*h2p_lw_hex_4_addr = hex_lut[quo10000]; // Turn off HEX
			*h2p_lw_hex_3_addr = hex_lut[quo1000]; // Turn off HEX
			*h2p_lw_hex_2_addr = hex_lut[quo100]; // Turn off HEX
			*h2p_lw_hex_1_addr = 0xFF; // Turn off HEX
			*h2p_lw_hex_1_addr = hex_lut[quo10]; // Turn off HEX
			*h2p_lw_hex_0_addr = hex_lut[rem10];	
			//printf("Switches = %d; Result of BCD convertion: %d %d %d %d %d %d \n",sw_reading, quo100000, quo10000, quo1000, quo100, quo10, rem10);
		} // end if
	}else{
		reset_peripherals_out(h2p_lw_hex_5_addr, h2p_lw_hex_4_addr, h2p_lw_hex_3_addr, h2p_lw_hex_2_addr, h2p_lw_hex_1_addr, h2p_lw_hex_0_addr, h2p_lw_leds_addr);
	} // end if
} // end number_switches

void led_flashing_workshop1(int *selector, volatile unsigned int *h2p_lw_switch_addr, volatile unsigned int *h2p_lw_leds_addr){
	volatile int prd;
	static unsigned char led_pattern = 0x01; // initial pattern
    unsigned long i, itr;
	if(*selector){
		printf("Funcionalidad del taller 1 en Ubuntu\n");
		*selector = 0;
	} // end if
	prd = *h2p_lw_switch_addr & 0x000003ff;
    led_pattern ^= 0X03; // toggle 2 LEDs (2LSBs)
    *h2p_lw_leds_addr = led_pattern;
    itr = prd*50000;
    for(i=0; i<itr;i++){} // dummy loop for delay
} // end led_flashing_workshop1

void reset_peripherals_out(volatile unsigned int *h2p_lw_hex_5_addr, volatile unsigned int *h2p_lw_hex_4_addr, volatile unsigned int *h2p_lw_hex_3_addr, volatile unsigned int *h2p_lw_hex_2_addr, volatile unsigned int *h2p_lw_hex_1_addr, volatile unsigned int *h2p_lw_hex_0_addr, volatile unsigned int *h2p_lw_leds_addr){
	*h2p_lw_leds_addr = 0;
	*h2p_lw_hex_5_addr = 0xFF; // Turn off HEX
	*h2p_lw_hex_4_addr = 0xFF; // Turn off HEX
	*h2p_lw_hex_3_addr = 0xFF; // Turn off HEX
	*h2p_lw_hex_2_addr = 0xFF; // Turn off HEX
	*h2p_lw_hex_1_addr = 0xFF; // Turn off HEX
	*h2p_lw_hex_0_addr = 0xFF; // Turn off HEX
} // end reset_peripherals_out

void sw_get_command(volatile unsigned int *h2p_lw_switch_addr, volatile unsigned int *h2p_lw_button_addr, int *prd, char *selector, char* sensitive_switch, char *start){
	int selector_temp = *selector;
	*prd = *h2p_lw_switch_addr;
	*selector = ((*prd & 0x00000200) >> 9);
	if(selector_temp != *selector){
		*sensitive_switch = 1;
	}else{
		*sensitive_switch = 0;
	}
	*start = is_fpga_button_pressed(0,h2p_lw_button_addr);
	*prd = *prd & 0x000001FF; // read flashing period from the first five switches
}// sw_get_command: Read Switches and key0

void led_chasing(volatile unsigned int* h2p_lw_leds_addr, int prd, char start, char *shift, char *flag_address){
    static unsigned long led_pattern = 0x01; // initial pattern
    unsigned long i, itr;
    //static char shift = -1, flag_address = 1; // flag_addres: 1 - Left | 0 - Right
    if(start == 0){
		*shift = 0;
    }else if(*flag_address){
		*shift += 1;
		if(*shift > 9){
			*flag_address = 0;
			*shift = 8;
		}
    }else{
		*shift -= 1;
		if(*shift < 0){
			*flag_address = 1;
			*shift = 1;
		}
    }
    led_pattern = (0x01 << *shift); // move the position of the on LED
    led_pattern |= 0x00;
    *h2p_lw_leds_addr = led_pattern;
    itr = prd*50000;
    for(i=0; i<itr;i++){} // dummy loop for delay
}// led_flash: point one

void led_collision(volatile unsigned int *h2p_lw_leds_addr, int prd, char start){
    static unsigned long led_pattern = 0x01; // initial pattern
    unsigned long i, itr;
    static char shift = -1, flag_address = 1; // flag_addres: 1 - shift_right: Left - shift_left: Right | 0 - shift_right: Right - shift_left : Left
    if(start == 0){
		shift = 0;
    }else if(flag_address){
		shift++;
		if(shift > 4){
			flag_address = 0;
			shift = 3;
		}
    }else{
		shift--;
		if(shift < 0){
			flag_address = 1;
			shift = 1;
		}
    }
    led_pattern = (0x01 << shift); // move the position of the on LED Right
    led_pattern |= (0x200 >> shift); // move the position of the on LED Left
    led_pattern |= 0x00;;
    *h2p_lw_leds_addr = led_pattern;
    itr = prd*50000;
    for(i=0; i<itr;i++){} // dummy loop for delay
}// led_collision: point two

void workshop2(int *selector_in, volatile unsigned int *h2p_lw_switch_addr, volatile unsigned int *h2p_lw_button_addr, volatile unsigned int *h2p_lw_hex_5_addr, volatile unsigned int *h2p_lw_hex_4_addr, volatile unsigned int *h2p_lw_hex_3_addr, volatile unsigned int *h2p_lw_hex_2_addr, volatile unsigned int *h2p_lw_hex_1_addr, volatile unsigned int *h2p_lw_hex_0_addr, volatile unsigned int *h2p_lw_leds_addr, char *shift, char *flag_address){
	int prd;
    char selector = 0, sensitive_switch, start;
    sw_get_command(h2p_lw_switch_addr, h2p_lw_button_addr, &prd, &selector, &sensitive_switch, &start);
    if(sensitive_switch){
		reset_peripherals_out(h2p_lw_hex_5_addr, h2p_lw_hex_4_addr, h2p_lw_hex_3_addr, h2p_lw_hex_2_addr, h2p_lw_hex_1_addr, h2p_lw_hex_0_addr, h2p_lw_leds_addr);
	}// sensitive_switch
	if(*selector_in){
		printf("Un poco de la funcionalidad del taller 2 en Ubuntu\n");
		*selector_in = 0;
	} // end if
	switch(selector){
		case 0:
			led_chasing(h2p_lw_leds_addr, prd, start, shift, flag_address);
			break;
        case 1:
			led_collision(h2p_lw_leds_addr, prd, start);
			break;
    }// switch: selector
} // end workshop2