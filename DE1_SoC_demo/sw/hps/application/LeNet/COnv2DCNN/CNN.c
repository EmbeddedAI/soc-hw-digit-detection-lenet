#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <math.h>
/* Measure execution time */
#include <time.h>

#include "alt_types.h"

// === FPGA side ===
#define HW_REGS_BASE ( 0xFF200000 )
#define HW_REGS_SPAN ( 0x00200000 ) 
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )
#define CNN_BASE 0x0
#define REG_AVALON_0_BASE 0x0

/*"register offset definitions */
#define CHARGE_DATA_REG_OFT 0 // charge_data register address offset
#define FINISH_DATA_REG_OFT 1 // finish_data register address offset
#define STROBE_REG_OFT 2 // strobe register address offset
#define TAKE_DATA_REG_OFT 3 // take_data register address offset
#define SYNC_CLR_REG_OFT 4 // sync clr register address offset
#define READY_REG_OFT 5 // data ready register address offset
#define DATA_BEGIN_OFT 6 // data begin address offset
#define DATA_QUANTY 10 // data quanty read/write
#define KERNEL_BEGIN_OFT 790 // data begin address offset
#define KERNEL_QUANTY 520 // data quanty read/write


float bitsToFloat(unsigned long numBin) {
    // 1 sign bit | 8 exponent bit | 23 fraction bits 
    float result=0,mantissa=1,numE=0;
    int i;
    for (i=0;i<=7;++i)
        numE += ((numBin & 1<<23+i)>>23+i)*pow(2,i);
    for (i=1;i<=23;++i)
        mantissa += ((numBin & 1<<23-i)>>23-i)*pow(2,-i);
    result=pow(2,numE-127)*mantissa;
    if(numBin & 1<<31)
        result *= -1.0;
    return result;
} // end bitsToFloat

/* main program */
int main(){
    // === FPGA Peripherals ===
    int fd; //Variable used to store /dev/mem pointer
    void *h2p_lw_virtual_base=NULL; //to store virtual base of HPS2FPGA lightweight bridge
    unsigned int *h2p_lw_cnn_addr=NULL; //to Store dot product reg address
    unsigned int *h2p_lw_cnn_data_addr=NULL;
    //=== get virtual addresses ===============
    // Open /dev/mem
	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf("ERROR: could not open \"/dev/mem\".\n");
        printf("    errno = %s\n", strerror(errno));
        exit(EXIT_FAILURE);
	}
	// get virtual addr that maps to physical address
	h2p_lw_virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );	
	if( h2p_lw_virtual_base == MAP_FAILED ) {
		printf("Error: h2p_lw_virtual_base mmap() failed.\n");
        printf("    errno = %s\n", strerror(errno));
        close(fd);
        exit(EXIT_FAILURE);
	} // end if
	// Get the address that maps to the FPGA peripherals
    h2p_lw_cnn_addr=(unsigned int *)(h2p_lw_virtual_base + CNN_BASE);
    unsigned long dataInput[25] = {0b01000011011001100000000000000000,0b01000011011000110000000000000000,0b01000011011000000000000000000000,0b01000011010111010000000000000000,0b01000011010110100000000000000000,0b01000011010010000000000000000000,0b01000011010001010000000000000000,0b01000011010000100000000000000000,0b01000011001111110000000000000000,0b01000011001111000000000000000000,0b01000011001010100000000000000000,0b01000011001001110000000000000000,0b01000011001001000000000000000000,0b01000011001000010000000000000000,0b01000011000111100000000000000000,0b01000011000011000000000000000000,0b01000011000010010000000000000000,0b01000011000001100000000000000000,0b01000011000000110000000000000000,0b01000011000000000000000000000000,0b01000010110111000000000000000000,0b01000010110101100000000000000000,0b01000010110100000000000000000000,0b01000010110010100000000000000000,0b01000010110001000000000000000000};
    unsigned long *dataOutput=(unsigned long*)calloc(DATA_QUANTY,sizeof(unsigned long));
    unsigned long busy,ready,result_dot;
    printf("Dot product accelerator test: \n\n");
    // while(1){
    //     busy=*(h2p_lw_cnn_addr+BUSY_REG_OFT);
    //     if(!busy)
    //         break;
    // }
    h2p_lw_cnn_data_addr=h2p_lw_cnn_addr+DATA_BEGIN_OFT;
    /* generate a 1-pulse */
    printf("Charge Data... \n");
    *(h2p_lw_cnn_addr+CHARGE_DATA_REG_OFT)=1;
    memcpy(h2p_lw_cnn_data_addr,dataInput,DATA_QUANTY*sizeof(unsigned long));
    /* generate a 1-pulse */
    printf("Finish Data... \n");
    *(h2p_lw_cnn_addr+FINISH_DATA_REG_OFT)=1;
    /* generate a 1-pulse */
    printf("Start... \n");
    long long nanoseconds;
    // Start measuring time
    struct timespec begin, end; 
    *(h2p_lw_cnn_addr+STROBE_REG_OFT)=1;
    clock_gettime(CLOCK_REALTIME, &begin);
    while(1){
        ready=*(h2p_lw_cnn_addr+READY_REG_OFT);
        if(ready){
            clock_gettime(CLOCK_REALTIME, &end);
            break;
        }
    }
    memcpy(dataOutput,h2p_lw_cnn_data_addr,DATA_QUANTY*sizeof(unsigned long));// result_dot=*(h2p_lw_cnn_addr+RESULT_DOT_REG_OFT);
    for(int i=0;i<DATA_QUANTY;++i){
        printf("Softmax[%d] = %f\n",i,bitsToFloat(dataOutput[i]));
    }
    *(h2p_lw_cnn_addr+READY_REG_OFT)=1;
    nanoseconds = end.tv_nsec - begin.tv_nsec;
    printf("Time (ns): %lld\n",nanoseconds);
    return 0;
}// main