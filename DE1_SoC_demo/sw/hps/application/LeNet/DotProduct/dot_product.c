#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <math.h>
/* Measure execution time */
#include <time.h>

#include "alt_types.h"

// === FPGA side ===
#define HW_REGS_BASE ( 0xFF200000 )
#define HW_REGS_SPAN ( 0x00200000 ) 
#define HW_REGS_MASK ( HW_REGS_SPAN - 1 )
#define DOT_PRODUCT_BASE 0x0
#define DIV32_BASE 0x80
#define HEX_5_BASE 0xa0
#define HEX_4_BASE 0xb0
#define HEX_3_BASE 0xc0
#define HEX_2_BASE 0xd0
#define HEX_1_BASE 0xe0
#define HEX_0_BASE 0xf0
#define BUTTONS_0_BASE 0x100
#define SWITCHES_0_BASE 0x110
#define LEDS_0_BASE 0x120
#define REG_AVALON_0_BASE 0x0

/*"register offset definitions */
#define STROBE_REG_OFT 0 // strobe register address offset
#define SYNC_CLR_REG_OFT 1 // sync clr register address offset
#define RESULT_DOT_REG_OFT 2 // result dot register address offset
#define BUSY_REG_OFT 3 // busy register address offset
#define READY_REG_OFT 4 // data ready register address offset
#define DATA_BEGIN_OFT 5 // data begin address offset
#define DATA_END_OFT 29 // data end address offset

float bitsToFloat(unsigned long numBin) {
    // 1 sign bit | 8 exponent bit | 23 fraction bits 
    float result=0,mantissa=1,numE=0;
    int i;
    for (i=0;i<=7;++i)
        numE += ((numBin & 1<<23+i)>>23+i)*pow(2,i);
    for (i=1;i<=23;++i)
        mantissa += ((numBin & 1<<23-i)>>23-i)*pow(2,-i);
    result=pow(2,numE-127)*mantissa;
    if(numBin & 1<<31)
        result *= -1.0;
    return result;
} // end bitsToFloat

/* main program */
int main(){
    // === FPGA Peripherals ===
    int fd; //Variable used to store /dev/mem pointer
    void *h2p_lw_virtual_base=NULL; //to store virtual base of HPS2FPGA lightweight bridge
    //volatile 
    unsigned int *h2p_lw_dot_product_addr=NULL; //to Store dot product reg address
    unsigned int *h2p_lw_dot_product_data_addr=NULL;
    //=== get virtual addresses ===============
    // Open /dev/mem
	fd = open("/dev/mem", O_RDWR | O_SYNC);
	if( ( fd = open( "/dev/mem", ( O_RDWR | O_SYNC ) ) ) == -1 ) {
		printf("ERROR: could not open \"/dev/mem\".\n");
        printf("    errno = %s\n", strerror(errno));
        exit(EXIT_FAILURE);
	}
	// get virtual addr that maps to physical address
	h2p_lw_virtual_base = mmap( NULL, HW_REGS_SPAN, ( PROT_READ | PROT_WRITE ), MAP_SHARED, fd, HW_REGS_BASE );	
	if( h2p_lw_virtual_base == MAP_FAILED ) {
		printf("Error: h2p_lw_virtual_base mmap() failed.\n");
        printf("    errno = %s\n", strerror(errno));
        close(fd);
        exit(EXIT_FAILURE);
	} // end if
	// Get the address that maps to the FPGA peripherals
    h2p_lw_dot_product_addr=(unsigned int *)(h2p_lw_virtual_base + DOT_PRODUCT_BASE);
    unsigned long dataInput[25] = {0b01000011011001100000000000000000,0b01000011011000110000000000000000,0b01000011011000000000000000000000,0b01000011010111010000000000000000,0b01000011010110100000000000000000,0b01000011010010000000000000000000,0b01000011010001010000000000000000,0b01000011010000100000000000000000,0b01000011001111110000000000000000,0b01000011001111000000000000000000,0b01000011001010100000000000000000,0b01000011001001110000000000000000,0b01000011001001000000000000000000,0b01000011001000010000000000000000,0b01000011000111100000000000000000,0b01000011000011000000000000000000,0b01000011000010010000000000000000,0b01000011000001100000000000000000,0b01000011000000110000000000000000,0b01000011000000000000000000000000,0b01000010110111000000000000000000,0b01000010110101100000000000000000,0b01000010110100000000000000000000,0b01000010110010100000000000000000,0b01000010110001000000000000000000};
    unsigned long *result_dot=(unsigned long*)calloc(1,sizeof(unsigned long));
    unsigned long busy,ready;//,result_dot,*ptr_result_dot=&result_dot;
    printf("Dot product accelerator test: \n\n");
    while(1){
        busy=*(h2p_lw_dot_product_addr+BUSY_REG_OFT);
        if(!busy)
            break;
    }
    h2p_lw_dot_product_data_addr=h2p_lw_dot_product_addr+DATA_BEGIN_OFT;
    memcpy(h2p_lw_dot_product_data_addr,dataInput,25*sizeof(unsigned long));
    /* generate a 1-pulse */
    printf("Start... \n");
    long long nanoseconds;
    // Start measuring time
    struct timespec begin, end; 
    *(h2p_lw_dot_product_addr+STROBE_REG_OFT)=1;
    clock_gettime(CLOCK_REALTIME, &begin);
    while(1){
        ready=*(h2p_lw_dot_product_addr+READY_REG_OFT);
        if(ready){
            clock_gettime(CLOCK_REALTIME, &end);
            break;
        }
    }
    h2p_lw_dot_product_data_addr=h2p_lw_dot_product_addr+RESULT_DOT_REG_OFT;
    memcpy(&result_dot[0],h2p_lw_dot_product_data_addr,sizeof(unsigned long));
    printf("Dot product = %f\n",bitsToFloat(result_dot[0]));
    *(h2p_lw_dot_product_addr+READY_REG_OFT)=1;
    nanoseconds = end.tv_nsec - begin.tv_nsec;
    printf("Time (ns): %lld\n",nanoseconds);
    return 0;
}// main