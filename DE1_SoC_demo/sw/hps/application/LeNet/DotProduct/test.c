#include <stdio.h>
#include <stdlib.h>

#define DATA_BEGIN_OFT 5 // data begin address offset
#define DATA_END_OFT 29 // data end address offset

/* main program */
int main(){

    float dataInput[25] = {230,227,224,221,218,200,197,194,191,188,170,167,164,161,158,140,137,134,131,128,110,107,104,101,98};
    printf("Division accelerator test: \n\n");
    for(int i = DATA_BEGIN_OFT;i<=DATA_END_OFT;++i){
        printf("%f ",dataInput[i-DATA_BEGIN_OFT]);
    }
    printf("\n");
    return 0;
}// main