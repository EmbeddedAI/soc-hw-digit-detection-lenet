# TCL File Generated by Component Editor 18.1
# Fri Jul 22 08:07:43 COT 2022
# DO NOT MODIFY


# 
# CNN "CNN" v1.0
#  2022.07.22.08:07:43
# 
# 

# 
# request TCL package from ACDS 16.1
# 
package require -exact qsys 16.1


# 
# module CNN
# 
set_module_property DESCRIPTION ""
set_module_property NAME CNN
set_module_property VERSION 1.0
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP LeNet
set_module_property AUTHOR ""
set_module_property DISPLAY_NAME CNN
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false


# 
# file sets
# 
add_fileset QUARTUS_SYNTH QUARTUS_SYNTH "" ""
set_fileset_property QUARTUS_SYNTH TOP_LEVEL CNN_avalon
set_fileset_property QUARTUS_SYNTH ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property QUARTUS_SYNTH ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file ReLU.vhd VHDL PATH soc_system/synthesis/submodules/ReLU.vhd
add_fileset_file accumulator.vhd VHDL PATH soc_system/synthesis/submodules/accumulator.vhd
add_fileset_file alt_add_sub.qip OTHER PATH soc_system/synthesis/submodules/alt_add_sub.qip
add_fileset_file alt_add_sub.vhd VHDL PATH soc_system/synthesis/submodules/alt_add_sub.vhd
add_fileset_file alt_float_div.hex HEX PATH soc_system/synthesis/submodules/alt_float_div.hex
add_fileset_file alt_float_div.qip OTHER PATH soc_system/synthesis/submodules/alt_float_div.qip
add_fileset_file alt_float_div.vhd VHDL PATH soc_system/synthesis/submodules/alt_float_div.vhd
add_fileset_file alt_float_exp.qip OTHER PATH soc_system/synthesis/submodules/alt_float_exp.qip
add_fileset_file alt_float_exp.vhd VHDL PATH soc_system/synthesis/submodules/alt_float_exp.vhd
add_fileset_file alt_float_mult.qip OTHER PATH soc_system/synthesis/submodules/alt_float_mult.qip
add_fileset_file alt_float_mult.vhd VHDL PATH soc_system/synthesis/submodules/alt_float_mult.vhd
add_fileset_file coder_buffer.vhd VHDL PATH soc_system/synthesis/submodules/coder_buffer.vhd
add_fileset_file coder_one_hot.vhd VHDL PATH soc_system/synthesis/submodules/coder_one_hot.vhd
add_fileset_file counter.vhd VHDL PATH soc_system/synthesis/submodules/counter.vhd
add_fileset_file data_conv2D.vhd VHDL PATH soc_system/synthesis/submodules/data_conv2D.vhd
add_fileset_file divisor.vhd VHDL PATH soc_system/synthesis/submodules/divisor.vhd
add_fileset_file divisor_avalon.vhd VHDL PATH soc_system/synthesis/submodules/divisor_avalon.vhd
add_fileset_file dot_product.vhd VHDL PATH soc_system/synthesis/submodules/dot_product.vhd
add_fileset_file dot_rows.vhd VHDL PATH soc_system/synthesis/submodules/dot_rows.vhd
add_fileset_file flatten_data.vhd VHDL PATH soc_system/synthesis/submodules/flatten_data.vhd
add_fileset_file flatten_data_mux.vhd VHDL PATH soc_system/synthesis/submodules/flatten_data_mux.vhd
add_fileset_file float_add_sub.vhd VHDL PATH soc_system/synthesis/submodules/float_add_sub.vhd
add_fileset_file float_comparator.vhd VHDL PATH soc_system/synthesis/submodules/float_comparator.vhd
add_fileset_file float_div.vhd VHDL PATH soc_system/synthesis/submodules/float_div.vhd
add_fileset_file float_exp.vhd VHDL PATH soc_system/synthesis/submodules/float_exp.vhd
add_fileset_file float_multiplier.vhd VHDL PATH soc_system/synthesis/submodules/float_multiplier.vhd
add_fileset_file ip_ram.qip OTHER PATH soc_system/synthesis/submodules/ip_ram.qip
add_fileset_file ip_ram.vhd VHDL PATH soc_system/synthesis/submodules/ip_ram.vhd
add_fileset_file mux2_1.vhd VHDL PATH soc_system/synthesis/submodules/mux2_1.vhd
add_fileset_file mux2_1_integer.vhd VHDL PATH soc_system/synthesis/submodules/mux2_1_integer.vhd
add_fileset_file mux_generic.vhd VHDL PATH soc_system/synthesis/submodules/mux_generic.vhd
add_fileset_file my_dff.vhd VHDL PATH soc_system/synthesis/submodules/my_dff.vhd
add_fileset_file or_reduction.vhd VHDL PATH soc_system/synthesis/submodules/or_reduction.vhd
add_fileset_file package_lenet.vhd VHDL PATH soc_system/synthesis/submodules/package_lenet.vhd
add_fileset_file softmax.vhd VHDL PATH soc_system/synthesis/submodules/softmax.vhd
add_fileset_file tiny_counter.vhd VHDL PATH soc_system/synthesis/submodules/tiny_counter.vhd
add_fileset_file rows_separate.vhd VHDL PATH soc_system/synthesis/submodules/rows_separate.vhd
add_fileset_file conv2D.vhd VHDL PATH soc_system/synthesis/submodules/conv2D.vhd
add_fileset_file CNN.vhd VHDL PATH soc_system/synthesis/submodules/CNN.vhd
add_fileset_file CNN_avalon.vhd VHDL PATH soc_system/synthesis/submodules/CNN_avalon.vhd TOP_LEVEL_FILE
add_fileset_file my_reg.vhd VHDL PATH soc_system/synthesis/submodules/my_reg.vhd


# 
# parameters
# 
add_parameter SIZE_ADDRESS INTEGER 12
set_parameter_property SIZE_ADDRESS DEFAULT_VALUE 12
set_parameter_property SIZE_ADDRESS DISPLAY_NAME SIZE_ADDRESS
set_parameter_property SIZE_ADDRESS TYPE INTEGER
set_parameter_property SIZE_ADDRESS UNITS None
set_parameter_property SIZE_ADDRESS ALLOWED_RANGES -2147483648:2147483647
set_parameter_property SIZE_ADDRESS HDL_PARAMETER true


# 
# display items
# 


# 
# connection point clock
# 
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true
set_interface_property clock EXPORT_OF ""
set_interface_property clock PORT_NAME_MAP ""
set_interface_property clock CMSIS_SVD_VARIABLES ""
set_interface_property clock SVD_ADDRESS_GROUP ""

add_interface_port clock clk clk Input 1


# 
# connection point reset
# 
add_interface reset reset end
set_interface_property reset associatedClock clock
set_interface_property reset synchronousEdges DEASSERT
set_interface_property reset ENABLED true
set_interface_property reset EXPORT_OF ""
set_interface_property reset PORT_NAME_MAP ""
set_interface_property reset CMSIS_SVD_VARIABLES ""
set_interface_property reset SVD_ADDRESS_GROUP ""

add_interface_port reset reset reset Input 1


# 
# connection point s_CNN
# 
add_interface s_CNN avalon end
set_interface_property s_CNN addressUnits WORDS
set_interface_property s_CNN associatedClock clock
set_interface_property s_CNN associatedReset reset
set_interface_property s_CNN bitsPerSymbol 8
set_interface_property s_CNN burstOnBurstBoundariesOnly false
set_interface_property s_CNN burstcountUnits WORDS
set_interface_property s_CNN explicitAddressSpan 0
set_interface_property s_CNN holdTime 0
set_interface_property s_CNN linewrapBursts false
set_interface_property s_CNN maximumPendingReadTransactions 0
set_interface_property s_CNN maximumPendingWriteTransactions 0
set_interface_property s_CNN readLatency 0
set_interface_property s_CNN readWaitTime 1
set_interface_property s_CNN setupTime 0
set_interface_property s_CNN timingUnits Cycles
set_interface_property s_CNN writeWaitTime 0
set_interface_property s_CNN ENABLED true
set_interface_property s_CNN EXPORT_OF ""
set_interface_property s_CNN PORT_NAME_MAP ""
set_interface_property s_CNN CMSIS_SVD_VARIABLES ""
set_interface_property s_CNN SVD_ADDRESS_GROUP ""

add_interface_port s_CNN s_address address Input size_address
add_interface_port s_CNN s_chipselect chipselect Input 1
add_interface_port s_CNN s_write write Input 1
add_interface_port s_CNN s_writedata writedata Input 32
add_interface_port s_CNN s_readdata readdata Output 32
set_interface_assignment s_CNN embeddedsw.configuration.isFlash 0
set_interface_assignment s_CNN embeddedsw.configuration.isMemoryDevice 0
set_interface_assignment s_CNN embeddedsw.configuration.isNonVolatileStorage 0
set_interface_assignment s_CNN embeddedsw.configuration.isPrintableDevice 0

