LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY coder_buffer IS 
	GENERIC 	(	DATA_WIDTH	:	INTEGER	:=	8); -- Define Generic Parameters
	PORT		(	selector		:	IN		STD_LOGIC;
					input_one	:	IN		STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
					input_two	:	IN		STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
					output_one	:	OUT	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
					output_two	:	OUT	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY coder_buffer; -- Entity 
--------------------------------------------------------
ARCHITECTURE rtl OF coder_buffer is 
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	--========================================
	--             MUX OUTPUT ONE
	--========================================
	mux_output_one:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	DATA_WIDTH) -- generic mapping
	PORT	MAP	(	x1					=>	input_one, -- 0
						x2					=>	input_two, -- 1
						sel				=>	selector,
						y					=>	output_one); -- port mapping
						
	--========================================
	--             MUX OUTPUT TWO
	--========================================
	mux_output_two:	ENTITY work.mux2_1
	GENERIC	MAP(	DATA_WIDTH		=>	DATA_WIDTH) -- generic mapping
	PORT	MAP	(	x1					=>	input_two, -- 0
						x2					=>	input_one, -- 1
						sel				=>	selector,
						y					=>	output_two); -- port mapping
END ARCHITECTURE rtl; -- Architecture