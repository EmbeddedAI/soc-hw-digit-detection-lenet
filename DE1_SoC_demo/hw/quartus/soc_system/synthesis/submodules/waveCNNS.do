onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /cnn_tb/DUT/rst
add wave -noupdate /cnn_tb/DUT/clk
add wave -noupdate /cnn_tb/DUT/syn_clr
add wave -noupdate /cnn_tb/DUT/charge_data
add wave -noupdate /cnn_tb/DUT/finish_data
add wave -noupdate /cnn_tb/DUT/strobe
add wave -noupdate /cnn_tb/DUT/take_data
add wave -noupdate /cnn_tb/DUT/data_ena
add wave -noupdate -radix unsigned /cnn_tb/DUT/address_in
add wave -noupdate -radix float32 /cnn_tb/DUT/data_in
add wave -noupdate -radix float32 /cnn_tb/DUT/data_out
add wave -noupdate /cnn_tb/DUT/data_ready
add wave -noupdate /cnn_tb/DUT/pr_state
add wave -noupdate /cnn_tb/DUT/nx_state
add wave -noupdate /cnn_tb/DUT/selector_buffer
add wave -noupdate /cnn_tb/DUT/wr_en_buffer_input
add wave -noupdate /cnn_tb/DUT/buffer_internal_data
add wave -noupdate /cnn_tb/DUT/buffer_one_data
add wave -noupdate /cnn_tb/DUT/buffer_two_data
add wave -noupdate /cnn_tb/DUT/buffer_output_data
add wave -noupdate /cnn_tb/DUT/buffer_output_data_layers
add wave -noupdate /cnn_tb/DUT/buffer_internal_rdaddress
add wave -noupdate /cnn_tb/DUT/buffer_internal_rdaddress_layers
add wave -noupdate /cnn_tb/DUT/buffer_one_rdaddress
add wave -noupdate /cnn_tb/DUT/buffer_two_rdaddress
add wave -noupdate /cnn_tb/DUT/buffer_input_rdaddress
add wave -noupdate /cnn_tb/DUT/buffer_input_rdaddress_layers
add wave -noupdate /cnn_tb/DUT/buffer_internal_wraddress
add wave -noupdate /cnn_tb/DUT/buffer_one_wraddress
add wave -noupdate /cnn_tb/DUT/buffer_two_wraddress
add wave -noupdate /cnn_tb/DUT/buffer_output_wraddress
add wave -noupdate /cnn_tb/DUT/buffer_output_wraddress_layers
add wave -noupdate /cnn_tb/DUT/buffer_internal_wren
add wave -noupdate /cnn_tb/DUT/buffer_one_wren
add wave -noupdate /cnn_tb/DUT/buffer_two_wren
add wave -noupdate /cnn_tb/DUT/buffer_input_wren
add wave -noupdate /cnn_tb/DUT/buffer_output_wren
add wave -noupdate /cnn_tb/DUT/buffer_output_wren_layers
add wave -noupdate /cnn_tb/DUT/buffer_internal_q
add wave -noupdate /cnn_tb/DUT/buffer_one_q
add wave -noupdate /cnn_tb/DUT/buffer_two_q
add wave -noupdate /cnn_tb/DUT/buffer_input_q
add wave -noupdate /cnn_tb/DUT/syn_clr_s
add wave -noupdate /cnn_tb/DUT/strobe_layer
add wave -noupdate /cnn_tb/DUT/strobe_layers
add wave -noupdate /cnn_tb/DUT/strobe_layers_s
add wave -noupdate /cnn_tb/DUT/data_ready_layers
add wave -noupdate /cnn_tb/DUT/selector_current_layer
add wave -noupdate /cnn_tb/DUT/softmax_layer/rst
add wave -noupdate /cnn_tb/DUT/softmax_layer/clk
add wave -noupdate /cnn_tb/DUT/softmax_layer/syn_clr
add wave -noupdate /cnn_tb/DUT/softmax_layer/strobe
add wave -noupdate -radix float32 /cnn_tb/DUT/softmax_layer/data_in
add wave -noupdate -radix unsigned /cnn_tb/DUT/softmax_layer/data_in_rdaddress
add wave -noupdate -radix float32 /cnn_tb/DUT/softmax_layer/data_out
add wave -noupdate /cnn_tb/DUT/softmax_layer/data_out_wraddress
add wave -noupdate /cnn_tb/DUT/softmax_layer/data_out_wren
add wave -noupdate /cnn_tb/DUT/softmax_layer/data_ready
add wave -noupdate /cnn_tb/DUT/softmax_layer/pr_state
add wave -noupdate /cnn_tb/DUT/softmax_layer/nx_state
add wave -noupdate /cnn_tb/DUT/softmax_layer/strobe_exp
add wave -noupdate /cnn_tb/DUT/softmax_layer/data_ready_exp
add wave -noupdate -radix float32 /cnn_tb/DUT/softmax_layer/result_exp
add wave -noupdate /cnn_tb/DUT/softmax_layer/set_data_b_acc
add wave -noupdate -radix float32 /cnn_tb/DUT/softmax_layer/result_acc
add wave -noupdate /cnn_tb/DUT/softmax_layer/data_ready_acc
add wave -noupdate /cnn_tb/DUT/softmax_layer/sel_exp_div
add wave -noupdate /cnn_tb/DUT/softmax_layer/enable_registers_s
add wave -noupdate /cnn_tb/DUT/softmax_layer/enable_registers
add wave -noupdate /cnn_tb/DUT/softmax_layer/data_exp_div_s
add wave -noupdate /cnn_tb/DUT/softmax_layer/data_exp_div
add wave -noupdate /cnn_tb/DUT/softmax_layer/data_result_div
add wave -noupdate /cnn_tb/DUT/softmax_layer/strobe_div
add wave -noupdate /cnn_tb/DUT/softmax_layer/data_ready_div
add wave -noupdate /cnn_tb/DUT/softmax_layer/flag_ready
add wave -noupdate /cnn_tb/DUT/softmax_layer/ena_sel
add wave -noupdate /cnn_tb/DUT/softmax_layer/syn_clr_sel
add wave -noupdate /cnn_tb/DUT/softmax_layer/selector
add wave -noupdate /cnn_tb/DUT/softmax_layer/selector_s
add wave -noupdate /cnn_tb/DUT/softmax_layer/selector_next
add wave -noupdate /cnn_tb/DUT/softmax_layer/SELECTOR_ONES
add wave -noupdate /cnn_tb/DUT/softmax_layer/SELECTOR_ZEROS_PADDING
add wave -noupdate /cnn_tb/DUT/softmax_layer/ONES_VALID
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {297668 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 560
configure wave -valuecolwidth 85
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {175201 ps} {839327 ps}
