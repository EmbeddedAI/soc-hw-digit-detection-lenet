LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK;
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY dot_rows IS 
	GENERIC	(	KERNEL_SIZE		:	INTEGER	:=	5;
					SIZE_SELECTOR	:	INTEGER	:=	5); -- Define Generic Parameters
	PORT		(	rst				:	IN		STD_LOGIC;
					clk				: 	IN 	STD_LOGIC;
					syn_clr			: 	IN 	STD_LOGIC;
					strobe			:	IN 	STD_LOGIC;
					data_a			:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
					data_b			:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
					result_dot		:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
					data_ready		:	OUT 	STD_LOGIC;
					busy				:	OUT 	STD_LOGIC); -- Define I/O Ports
END ENTITY dot_rows; -- Entity 
--------------------------------------------------------
ARCHITECTURE arch_dot_rows OF dot_rows is 
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	result_mult			:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL	data_ready_mult	:	STD_LOGIC;
	SIGNAL	busy_mult			:	STD_LOGIC;
	SIGNAL	busy_acc				:	STD_LOGIC;
	SIGNAL	set_data_b			:	STD_LOGIC;
	SIGNAL	data_b_acc			:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
BEGIN 
	--========================================
	--                CIRCUIT               
	--========================================
	set_data_b	<=	'0';
	data_b_acc	<=	"00000000000000000000000000000000";

	--========================================
	--            MULTIPLIER FLOAT
	--========================================
	multiplier: ENTITY work.float_multiplier
	GENERIC	MAP	(	DATA_WIDTH	=>	BITS_FLOAT,
							LATENCY		=>	FLOAT_MULT_LATENCY) -- generic mapping
	PORT	MAP		(	rst			=> rst,
							clk			=> clk,
							syn_clr		=> syn_clr, 
							strobe		=> strobe,
							dataa			=> data_a,
							datab			=> data_b,
							result		=> result_mult,
							data_ready	=> data_ready_mult,
							busy			=>	busy_mult); -- port mapping

	--========================================
	--              ACCUMULATOR
	--========================================
	acc: ENTITY work.accumulator
	PORT	MAP	(	rst				=> rst,
						clk				=> clk,
						syn_clr			=> syn_clr,
						set_data_b		=> set_data_b,
						strobe			=> data_ready_mult,
						data_a			=> result_mult,
						data_b			=> data_b_acc,
						result_acc		=> result_dot,
						data_ready		=> data_ready,
						busy				=>	busy_acc); -- port mapping

	--========================================
	--                 OUTPUT
	--========================================
	busy	<=	busy_acc OR busy_mult OR strobe;
END ARCHITECTURE arch_dot_rows; -- Architecture