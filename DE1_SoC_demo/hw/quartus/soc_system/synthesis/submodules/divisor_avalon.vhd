LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY divisor_avalon IS 
	GENERIC 	(	W						:	INTEGER	:=	32;
					CBIT					:	INTEGER	:=	6); -- CBIT=log2(W)+1
	PORT	(		-- TO BE CONNECTED TO AVALON CLOCK INPUT INTERFACE
					clk					:	IN		STD_LOGIC;
					reset					:	IN		STD_LOGIC;
					-- TO BE CONNECTED TO AVALON MM SLAVE INTERFACE
					div_address			:	IN		STD_LOGIC_VECTOR(2 DOWNTO 0);
					div_chipselect		:	IN		STD_LOGIC;
					div_write			:	IN		STD_LOGIC;
					div_writedata		:	IN		STD_LOGIC_VECTOR(W-1 DOWNTO 0);
					div_readdata		:	OUT	STD_LOGIC_VECTOR(W-1 DOWNTO 0);
					-- TO BE CONNECTED TO AVALON INTERRUPT SENDER INTERFACE
					div_irq				:	OUT	STD_LOGIC;
					-- TO BE CONNECTED TO AVALON CONDUIT INTERFACE
					div_led				:	OUT	STD_LOGIC_VECTOR(9 DOWNTO 0)); -- Define I/O Ports
END ENTITY divisor_avalon; -- Entity 
--------------------------------------------------------
ARCHITECTURE avalonMMslave OF divisor_avalon is 
	--CONSTANTS
	CONSTANT	ZEROS				:	STD_LOGIC_VECTOR(W-2 DOWNTO 0) := (OTHERS => '0');
	--SIGNALS
	SIGNAL	div_start		:	STD_LOGIC;
	SIGNAL	div_ready		:	STD_LOGIC;
	SIGNAL 	set_done_tick	:	STD_LOGIC;
	SIGNAL	clr_done_tick	: 	STD_LOGIC;
	SIGNAL	dvnd_reg			:	STD_LOGIC_VECTOR(W-1 DOWNTO 0);
	SIGNAL	dvsr_reg			:	STD_LOGIC_VECTOR(W-1 DOWNTO 0);
	SIGNAL 	done_tick_reg	:	STD_LOGIC;
	SIGNAL	quo				:	STD_LOGIC_VECTOR(W-1 DOWNTO 0);
	SIGNAL	rmd				:	STD_LOGIC_VECTOR(W-1 DOWNTO 0);
	SIGNAL 	wr_en				:	STD_LOGIC;
	SIGNAL 	wr_dvnd			:	STD_LOGIC;
	SIGNAL 	wr_dvsr			:	STD_LOGIC;
	SIGNAL 	flag_dvnd_reg	:	STD_LOGIC;
	SIGNAL	flag_dvsr_reg	:	STD_LOGIC;
BEGIN 
	--========================================
	--         DIVISOR INSTANTIATION        
	--========================================
	div_unit:	ENTITY work.divisor
	GENERIC	MAP(	W		=>	W,
						CBIT	=>	CBIT) -- generic mapping
	PORT	MAP	(	clk			=>	clk,
						reset			=>	'0',
						start			=>	div_start,
						dvnd			=>	dvnd_reg,
						dvsr			=>	dvsr_reg,
						ready			=>	div_ready,
						done_tick	=>	set_done_tick,
						quo			=>	quo,
						rmd			=>	rmd); -- port mapping

	--========================================
	--               REGISTERS        
	--========================================						
	PROCESS( clk, reset)
	BEGIN
		IF (reset = '1') THEN
			dvnd_reg			<=	(OTHERS => '0');
			dvsr_reg			<=	(OTHERS => '0');
			done_tick_reg	<= '0';
			flag_dvnd_reg	<= '0';
			flag_dvsr_reg	<= '0';
		ELSIF(rising_edge(clk)) THEN
			------------------------------------
			IF (wr_dvnd = '1') THEN
				dvnd_reg		<=	div_writedata;
				flag_dvnd_reg	<= '1';
			END IF;
			------------------------------------
			IF (wr_dvsr = '1') THEN
				dvsr_reg		<=	div_writedata;
				flag_dvsr_reg	<= '1';
			END IF;
			------------------------------------
			IF	(set_done_tick = '1') THEN
				done_tick_reg	<= '1';
			ELSIF (clr_done_tick = '1') THEN
				done_tick_reg	<= '0';
			END IF;
		END IF;
	END PROCESS;
	
	--========================================
	--        WRITE DECODING LOGIC 
	--========================================
	wr_en				<=	'1'	WHEN	(div_write = '1' AND div_chipselect = '1')	ELSE	'0';
	wr_dvnd			<=	'1'	WHEN	(div_address = "000" AND wr_en = '1')	ELSE	'0'; -- offset 0 (dividend register)
	wr_dvsr			<=	'1'	WHEN	(div_address = "001" AND wr_en = '1')	ELSE	'0'; -- offset 1 (divisor register)
	div_start		<=	'1'	WHEN	(div_address = "010" AND wr_en = '1')	ELSE	'0'; -- offset 2 (start register)
	clr_done_tick	<=	'1'	WHEN	(div_address = "110" AND wr_en = '1')	ELSE	'0'; -- offset 6 (done_tick register)
	
	--========================================
	--  READ MULTIPLEXING LOGIC (assume W=32)
	--========================================
	div_readdata	<=	quo 							WHEN	div_address = "011"	ELSE	-- offset 3 (quotient register)
							rmd							WHEN	div_address = "100"	ELSE	-- offset 4 (remainder register)
							ZEROS & div_ready       WHEN	div_address = "101"	ELSE	-- offset 5 (ready register)
																										--    bit 0: ready status
							ZEROS & done_tick_reg;												-- offset 6 (done_tick register)
																										--    bit 0: done_tick flag
																										
	--========================================
	--           CONDUIT SIGNAL
	--========================================
	div_led(6 DOWNTO 0)	<= rmd(6 DOWNTO 0); -- assume that W > 7
	div_led(7)				<=	div_ready;
	div_led(8)				<=	flag_dvnd_reg;
	div_led(9)				<=	flag_dvsr_reg;
	--========================================
	--      INTERRUPT REQUEST SIGNAL
	--========================================
	div_irq	<=	done_tick_reg;
END ARCHITECTURE avalonMMslave; -- Architecture
