LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY ReLU IS 
	PORT	(	data_in	:	IN		STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
				data_out	:	OUT	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY ReLU; -- Entity 
--------------------------------------------------------
ARCHITECTURE rtl OF ReLU is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT ZEROS	:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0)	:=	(OTHERS => '0');
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	data_out	<= data_in	WHEN	data_in(BITS_FLOAT-1)='0' ELSE	
					ZEROS;
END ARCHITECTURE rtl; -- Architecture