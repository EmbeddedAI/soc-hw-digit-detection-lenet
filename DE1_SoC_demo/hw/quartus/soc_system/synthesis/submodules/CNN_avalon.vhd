LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
USE IEEE.NUMERIC_STD.ALL;
LIBRARY WORK; -- WORK library is included
USE work.package_lenet.all;
--------------------------------------------------------
ENTITY CNN_avalon IS 
	GENERIC	(	SIZE_ADDRESS	:	INTEGER	:=	ADDR_BUFFERS_WIDTH); -- Define Generic Parameters
	PORT		(	-- TO BE CONNECTED TO AVALON CLOCK INPUT INTERFACE
					clk				:	IN		STD_LOGIC;
					reset				:	IN		STD_LOGIC;
					-- TO BE CONNECTED TO AVALON MM SLAVE INTERFACE
					s_address		:	IN		STD_LOGIC_VECTOR(SIZE_ADDRESS-1 DOWNTO 0);
					s_chipselect	:	IN		STD_LOGIC;
					s_write			:	IN		STD_LOGIC;
					s_writedata		:	IN		STD_LOGIC_VECTOR(BITS_AVALON-1 DOWNTO 0);
					s_readdata		:	OUT	STD_LOGIC_VECTOR(BITS_AVALON-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY CNN_avalon; -- Entity 
--------------------------------------------------------
ARCHITECTURE avalonMMslave OF CNN_avalon is 
	--========================================
	--                CONSTANTS               
	--========================================
	CONSTANT	ZEROS				:	STD_LOGIC_VECTOR(BITS_AVALON-2 DOWNTO 0) := (OTHERS => '0');
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	s_strobe			:	STD_LOGIC;
	SIGNAL	s_data_ready	:	STD_LOGIC;
	SIGNAL	s_syn_clr		:	STD_LOGIC;
	SIGNAL	s_charge_data	:	STD_LOGIC;
	SIGNAL	s_finish_data	:	STD_LOGIC;
	SIGNAL	s_take_data		:	STD_LOGIC;
	SIGNAL	set_syn_clr		:	STD_LOGIC;
	SIGNAL 	set_data_ready	:	STD_LOGIC;
	SIGNAL	clr_data_ready	: 	STD_LOGIC;
	SIGNAL	s_data_out		:	STD_LOGIC_VECTOR(BITS_FLOAT-1 DOWNTO 0);
	SIGNAL 	wr_en				:	STD_LOGIC;
	SIGNAL	data_en			:	STD_LOGIC;
	SIGNAL	s_address_s		:	STD_LOGIC_VECTOR(SIZE_ADDRESS-1 DOWNTO 0);
BEGIN 
	--========================================
	--                CIRCUIT               
	--========================================
	--========================================
	--                  CNN
	--========================================
	CNN_unit: ENTITY work.CNN
	PORT	MAP	(	rst				=>	reset,
						clk				=>	clk,
						syn_clr			=>	s_syn_clr,
						charge_data		=>	s_charge_data,
						finish_data		=>	s_finish_data,
						strobe			=>	s_strobe,
						take_data		=>	s_take_data,
						data_ena			=>	data_en,
						address_in		=>	s_address_s,
						data_in			=> s_writedata,
						data_out			=>	s_data_out,
						data_ready		=>	set_data_ready); -- port mapping

	--========================================
	--               REGISTERS        
	--========================================						
	PROCESS( clk, reset)
	BEGIN
		IF (reset = '1') THEN
			s_data_ready			<=	'0';
			s_syn_clr				<=	'0';
		ELSIF(rising_edge(clk)) THEN
			------------------------------------
			IF	(set_syn_clr = '1') THEN
				s_syn_clr			<= s_writedata(0);
			END IF;
			------------------------------------
			IF	(set_data_ready = '1') THEN
				s_data_ready		<= '1';
			ELSIF (clr_data_ready = '1') THEN
				s_data_ready		<= '0';
			END IF;
		END IF;
	END PROCESS;
	
	--========================================
	--        WRITE DECODING LOGIC 
	--========================================
	wr_en				<=	'1'	WHEN	(s_write = '1' AND s_chipselect = '1')	ELSE	'0';
	s_charge_data	<=	'1'	WHEN	(s_address = "000000000000" AND wr_en = '1')	ELSE	'0'; -- offset 0 (charge_data register)
	s_finish_data	<=	'1'	WHEN	(s_address = "000000000001" AND wr_en = '1')	ELSE	'0'; -- offset 1 (finish_data register)
	s_strobe			<=	'1'	WHEN	(s_address = "000000000010" AND wr_en = '1')	ELSE	'0'; -- offset 2 (strobe register)
	s_take_data		<=	'1'	WHEN	(s_address = "000000000011" AND wr_en = '1')	ELSE	'0'; -- offset 3 (take_data register)
	set_syn_clr		<=	'1'	WHEN	(s_address = "000000000100" AND wr_en = '1')	ELSE	'0'; -- offset 4 (sync_clr register)
	clr_data_ready	<=	'1'	WHEN	(s_address = "000000000101" AND wr_en = '1')	ELSE	'0'; -- offset 5 (clr_data_ready register)
	data_en			<=	'1'	WHEN	(s_address >="000000000110" AND wr_en = '1')	ELSE	'0'; -- offset 6-789 (data registers)
	s_address_s		<= STD_LOGIC_VECTOR(UNSIGNED(s_address)-6)	WHEN 	(s_address >="000000000110")	ELSE	(OTHERS => '0');

	--========================================
	--  READ MULTIPLEXING LOGIC 
	--========================================
	s_readdata		<=	s_data_out				WHEN	(s_address >="000000000110" AND wr_en = '0')	ELSE 	-- offset 6-789 (data registers)
							ZEROS & s_data_ready	WHEN	s_address = "000000000101"							ELSE 	-- offset 5 (s_data_ready register)
							(OTHERS	=>	'0');																					-- bit 0: s_data_ready flag
END ARCHITECTURE avalonMMslave; -- Architecture