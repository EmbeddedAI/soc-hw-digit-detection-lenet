LIBRARY IEEE; -- IEEE library is included
USE ieee.std_logic_1164.all; -- the std_logic_1164 package from the IEEE library is used
--------------------------------------------------------
ENTITY mux2_1 IS 
	GENERIC(	DATA_WIDTH	:	INTEGER	:= 8);
	PORT	 (	x1				:	IN		STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
				x2				:	IN		STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0);
				sel			:	IN		STD_LOGIC;
				y				:	OUT	STD_LOGIC_VECTOR(DATA_WIDTH-1 DOWNTO 0)); -- Define I/O Ports
END ENTITY mux2_1; -- Entity 
--------------------------------------------------------
ARCHITECTURE functional OF mux2_1 is 
BEGIN 
	--========================================
	--                 CIRCUIT
	--========================================
	y	<=	x2	WHEN	sel = '1'	ELSE
			x1;
END ARCHITECTURE functional; -- Architecture