-- altera vhdl_input_version vhdl_2008
----------------------------------------
LIBRARY IEEE;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;
LIBRARY WORK;
USE work.package_lenet.all;
----------------------------------
ENTITY counter IS
	GENERIC	(	MAXCOUNTER_WIDTH	:	INTEGER	:=	MAX_COUNTER_WIDTH);
	PORT		(	clk					:	IN		STD_LOGIC;
					rst					:	IN		STD_LOGIC;
					ena 					:	IN		STD_LOGIC;
					syn_clr				:	IN		STD_LOGIC;
					clr_counter			:	IN		STD_LOGIC;
					max_counter			:	IN		UNSIGNED(MAXCOUNTER_WIDTH-1 DOWNTO 0);
					counter_u			:	OUT	UNSIGNED(MAXCOUNTER_WIDTH-1 DOWNTO 0);
					counter_std			:	OUT	STD_LOGIC_VECTOR(MAXCOUNTER_WIDTH-1 DOWNTO 0);
					max_tick				:	OUT	STD_LOGIC);
END ENTITY counter;
-------------------------------------
ARCHITECTURE rtl OF counter IS
	--========================================
	--                 SIGNALS                
	--========================================
	SIGNAL	max_tick_s		:	STD_LOGIC;
	SIGNAL	count_s			:	UNSIGNED(MAXCOUNTER_WIDTH-1 DOWNTO 0);
	SIGNAL	count_next		:	UNSIGNED(MAXCOUNTER_WIDTH-1 DOWNTO 0);
BEGIN
	--========================================
	--                 CIRCUIT
	--========================================	
	--========================================
	--              COUNTER LOGIC
	--========================================
	count_next 	<=		(OTHERS =>	'0') 	WHEN 	syn_clr			='1' 		ELSE
							(OTHERS =>	'0') 	WHEN 	clr_counter		='1' 		ELSE
							(OTHERS =>	'0') 	WHEN 	max_tick_s		='1'		ELSE
							count_s + 1;

	--========================================
	--                MAX TICK
	--========================================
	max_tick_s	<=	'1' when count_s = max_counter	ELSE '0';

	--========================================
	--            REGISTER COUNTER
	--========================================	
	regCounter: PROCESS(ena, clk, rst)
	BEGIN
		IF (rst = '1') THEN
			count_s <= (OTHERS => '0');
		ELSIF(rising_edge(clk)) THEN
			IF (ena = '1') THEN
				count_s <= count_next;
			END IF;
		END IF;
	END PROCESS;

	--========================================
	--                 OUTPUT
	--========================================
	max_tick		<=	max_tick_s;
	counter_u	<=	count_s;
	counter_std	<=	STD_LOGIC_VECTOR(count_s);

END ARCHITECTURE rtl;